const assert = require('assert');

function sumValidation(sumSterling,sumEuro, sterlingProd, euroProd) {
    assert(sumSterling[1].toString() === sumEuro[1].toString(), "MSG: Total of Variety1 is incorrect");
    assert(sumSterling[2].toString() === sumEuro[2].toString(), "MSG: Total of Variety2 is incorrect");
    assert(sumSterling[3].toString() === sumEuro[3].toString(), "MSG: Total of Variety3 is incorrect");
    assert(sumSterling[4].toString() === sumEuro[4].toString(), "MSG: Total of Variety4 is incorrect");
    // console.log("Sum of all the variety's total of Table A matches Table B");

    assert(sumSterling[1].toString() === sterlingProd[5][1].toString(), "MSG: Sterling Total of Variety1 is incorrect");
    assert(sumSterling[2].toString() === sterlingProd[5][2].toString(), "MSG: Sterling Total of Variety2 is incorrect");
    assert(sumSterling[3].toString() === sterlingProd[5][3].toString(), "MSG: Sterling Total of Variety3 is incorrect");
    assert(sumSterling[4].toString() === sterlingProd[5][4].toString(), "MSG: Sterling Total of Variety4 is incorrect");
    // console.log("Sum of all the Sterling total value is equal to the sum of the values in the corresponding column");

    assert(sumEuro[1] === euroProd[5][1]/1.5, "MSG: Euro Total of Variety1 is incorrect");
    assert(sumEuro[2] === euroProd[5][2]/1.5, "MSG: Euro Total of Variety2 is incorrect");
    assert(sumEuro[3] === euroProd[5][3]/1.5, "MSG: Euro Total of Variety3 is incorrect");
    assert(sumEuro[4] === euroProd[5][4]/1.5, "MSG: Euro Total of Variety4 is incorrect");
    // console.log("Sum of all the Euro total value is equal to the sum of the values in the corresponding column");
}
module.exports.sumValidation = sumValidation;

function emptyCells(sterlingProd, euroProd) {
    assert(sterlingProd[2][4] === "" && euroProd[2][4] === "", "MSG: Product2, Variety4 cell contains value");
    assert(sterlingProd[3][3] === "" && euroProd[3][3] === "", "MSG: Product3, Variety3 cell contains value");
    assert(sterlingProd[3][4] === "" && euroProd[3][4] === "", "MSG: Product3, Variety4 cell contains value");
    assert(sterlingProd[4][2] === "" && euroProd[4][2] === "", "MSG: Product4, Variety2 cell contains value");
    assert(sterlingProd[4][3] === "" && euroProd[4][3] === "", "MSG: Product4, Variety3 cell contains value");
    assert(sterlingProd[4][4] === "" && euroProd[4][4] === "", "MSG: Product4, Variety4 cell contains value");
    // console.log("No value found on the shaded cells as expected");
}
module.exports.emptyCells = emptyCells;

function tableComparison(sterlingProd, euroProd) {
    assert(sterlingProd[1][1].toString() === euroProd[1][1].toString(), "MSG: Mismatch with Product1, Variety1");
    assert(sterlingProd[1][2].toString() === euroProd[1][2].toString(), "MSG: Mismatch with Product1, Variety2");
    assert(sterlingProd[1][3].toString() === euroProd[1][3].toString(), "MSG: Mismatch with Product1, Variety3");
    assert(sterlingProd[1][4].toString() === euroProd[1][4].toString(), "MSG: Mismatch with Product1, Variety4");
    // console.log("All the Product1 Variety values of Table B matches Table A");

    assert(sterlingProd[2][1].toString() === euroProd[2][1].toString(), "MSG: Mismatch with Product2, Variety1");
    assert(sterlingProd[2][2].toString() === euroProd[2][2].toString(), "MSG: Mismatch with Product2, Variety2");
    assert(sterlingProd[2][3].toString() === euroProd[2][3].toString(), "MSG: Mismatch with Product2, Variety3");
    // console.log("All the Product2 Variety values of Table B matches Table A");

    assert(sterlingProd[3][1].toString() === euroProd[3][1].toString(), "MSG: Mismatch with Product3, Variety1");
    assert(sterlingProd[3][2].toString() === euroProd[3][2].toString(), "MSG: Mismatch with Product3, Variety2");
    // console.log("All the Product3 Variety values of Table B matches Table A");

    assert(sterlingProd[4][1].toString() === euroProd[4][1].toString(), "MSG: Mismatch with Product4, Variety1");
    // console.log("All the Product4 Variety values of Table B matches Table A");
}
module.exports.tableComparison = tableComparison;

