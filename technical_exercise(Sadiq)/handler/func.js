
const fs = require('fs');
const assertion = require('../handler/assestion');
const rate = x => x / 1.5;
const sterling = './datatable/sterling.csv';
const euro = './datatable/euro.csv';

function tableComparison(sterlingProd, euroProd) {
    fs.readFile(sterling, 'utf8', (err, data) => {
        if (err) throw err;
        sterlingProd = data.split('\n').map((row) => {
            return row.split(",");
        });

        fs.readFile(euro, 'utf8', (err, data) => {
            if (err) throw err;
            euroProd = data.split('\n').map((row) => {
                return row.split(",");
            }).map(variety => variety.map(rate));
            assertion.tableComparison(sterlingProd, euroProd);
        });
    });
}
module.exports.tableComparison = tableComparison;

function emptyCells(sterlingProd, euroProd) {
    fs.readFile(sterling, 'utf8', (err, data) => {
        if (err) throw err;
        sterlingProd = data.split('\n').map((row) => {
            return row.split(",");
        });

        fs.readFile(euro, 'utf8', (err, data) => {
            if (err) throw err;
            euroProd = data.split('\n').map((row) => {
                return row.split(",");
            });
            assertion.emptyCells(sterlingProd, euroProd);
        });
    });
};
module.exports.emptyCells = emptyCells;

function sumValidation(sterlingProd, euroProd, sumSterling, sumEuro) {
    fs.readFile(sterling, 'utf8', (err, data) => {
        if (err) throw err;
        sterlingProd = data.split('\n').map((row) => {
            return row.split(",").map(Number);
        });
        sumSterling = sterlingProd[1].map((num, add) => {
            return num + sterlingProd[2][add];
        }).map((num, add) => {
            return num + sterlingProd[3][add];
        }).map((num, add) => {
            return num + sterlingProd[4][add];
        })

        fs.readFile(euro, 'utf8', (err, data) => {
            if (err) throw err;
            euroProd = data.split('\n').map((row) => {
                return row.split(",").map(Number);
            });
            sumEuro = euroProd[1].map((num, add) => {
                return num + euroProd[2][add];
            }).map((num, add) => {
                return num + euroProd[3][add];
            }).map((num, add) => {
                return num + euroProd[4][add];
            }).map(rate);
            assertion.sumValidation(sumSterling, sumEuro, sterlingProd, euroProd);
        });
    });
};
module.exports.sumValidation = sumValidation;