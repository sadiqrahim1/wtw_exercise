
const func = require('./handler/func');

describe('Technical Exercise for WTW', () => {

    test('Compare the table B values with table A, which validates with various assertions', () => {
        func.tableComparison();
    }, 10000);

    test('Test the empty cells, which validates with various assertions', () => {
        func.emptyCells();
    }, 10000);

    test('Test the total of each varieties (Last row of both tables), which validates with various assertions', () => {
        func.sumValidation();
    }, 10000);
});